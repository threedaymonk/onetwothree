class Evaluator
  def initialize(context)
    @context = context
    @pending = []
  end

  def <<(line)
    @pending << line
  end

  def evaluate(lineno)
    eval(@pending.join, @context, "stdin", lineno).inspect
  rescue StandardError, SyntaxError => e
    "#{e.class}: #{e}"
  ensure
    @pending.replace []
  end
end

evaluator = Evaluator.new(binding)

lineno = 0
mode = :text
ARGF.each_line do |line|
  lineno += 1
  case line
  when /^\`\`\`(\w+)$/
    puts line
    mode = $1
  when /^\`\`\`$/
    puts line
    mode = :text
    evaluator.evaluate lineno
  else
    if mode == "ruby"
      evaluator << line
      if line =~ /# =>/
        puts line.sub(/# =>.*/, "# => #{evaluator.evaluate(lineno)}")
      else
        puts line
      end
    else
      puts line
    end
  end
end
