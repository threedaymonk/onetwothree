class Object
  def numberwang?; false; end
end

class Numeric
  def numberwang?; true; end
end

class String
  alias_method :plus, :+

  def coerce(lhs)
    if numberwang?
      [lhs, to_number]
    else
      [lhs.to_s, self]
    end
  end

  def numberwang?
    match(/\A(?:\d+(?:\.\d+)?|0x[0-9a-f]+)\z/)
  end

  def to_number
    match(/\./) ? Float(self) : Integer(self)
  end

  def +(rhs)
    if numberwang? && rhs.numberwang?
      to_number + rhs
    else
      plus(rhs.to_s)
    end
  end

  def -(rhs)
    if numberwang? && rhs.numberwang?
      to_number - rhs
    else
      raise "can't subtract strings"
    end
  end
end

require "minitest/spec"
require "minitest/autorun"

describe "Numberwang" do
  examples = {
    ->{ 1 + "2" } => 3,
    ->{ "1" + 2 } => 3,
    ->{ "1" + "2" } => 3,
    ->{ "foo" + "bar" } => "foobar",
    ->{ "foo" + 1 } => "foo1",
    ->{ 1 + "foo" } => "1foo",
    ->{ 1.2 + "3.4" } => 4.6,
    ->{ "1.2" + 3.4 } => 4.6,
    ->{ "1.2" + "3.4" } => 4.6,
    ->{ 0x10 + "0x08" } => 24,
    ->{ "0x10" + 0x08 } => 24,
    ->{ "0x10" + "0x08" } => 24,
    ->{ 3 - "2" } => 1,
    ->{ "3" - 2 } => 1,
    ->{ "3" - "2" } => 1
  }

  examples.each_with_index do |(expr, res), i|
    it "should evaluate expression #{i}" do
      expr.call.must_equal res
    end
  end
end
