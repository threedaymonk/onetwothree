```ruby
require "date"

name  = "Paul Battley"
event = "LRUG"
date  = Date.new(2013, 2, 11)

1 + 2 == 3 # => true
```

Hi. All right, I'm here to tell you that one plus two equals three. You
probably knew that.
I mean, most languages work that way, except for LISPs, in which it's
`(= (+ 1 2) 3)`, or FORTH, which is probably `1 2 + 3 =` or something like
that.

But what happens if 2 is a string? You probably already know.

----

```ruby
1 + "2"
# => TypeError: String can't be coerced into Fixnum
```

Well, it works in Perl.
It works in PHP.
JavaScript thinks that "12" is a good answer, but well, that's JavaScript, the
most special of languages.

Can we fix it?

----

Yes, we can.

The hint is there in the word "coerced".

Let's step back a bit. What happens when you do 1 + 2 in Ruby?
The `+` operator is really syntactic sugar.
There's a `+` method defined on 1 (in fact, on Fixnum, but that's not relevant
right now).

----

```ruby
1 + 2  # => 3

1.+(2) # => 3
```

Give it a Fixnum, and it will add the right hand side and return the result.
But if it's not a Fixnum, it attempts to "coerce" it.
At this point, it's probably more helpful if we go all Smalltalk and talk about
"sending a message" instead of "calling a method".

----

```ruby
1 + x
1.+(x)
a, b = x.coerce(1)
a.+(b)
```

If `+` doesn't know what to do with the right hand side, it sends `coerce` to
it with the left hand side as a parameter.
`coerce` returns two values.
The original message, namely `+`, is sent to the first value with the
second as a parameter.
The result of this is then returned to the original caller.

----

```ruby
class String
  alias_method :to_number, :to_i

  def coerce(lhs)
    [lhs, to_number]
  end
end
```

So all we need to do is to define a `coerce` method on String!
When we try to add a string to a number, it will be turned into an integer
and that added instead - and Fixnums know how to handle that.

----

```ruby
1 + "2" # => 3
```

Hurrah. Job done.
But is it?
What happens if we try it the other way round?

----

```ruby
"1" + 2
# => TypeError: can't convert Fixnum into String

"1" + "2" # => "12"
```

Well, we can add a string to a Fixnum, but we still can't add a Fixnum to a
string.
String's implementation of `+` is complaining, because it doesn't know how to
append a number, but we don't _want_ it to append: we want it to add it if it's
a number.

Adding one and two and getting twelve is pretty unfortunate, too.

----

```ruby
class String
  def numberwang?
    match(/\A\d+\z/)
  end
end
```

We need to know if our string is a string or a number masquerading as a string.
Let's give String a `numberwang?` property that we can use.
This is obviously not completely fit for purpose, because it doesn't work with
negative numbers, floats, hex, … but it's a start!

----

```ruby
class String
  alias_method :plus, :+

  def +(rhs)
    if numberwang?
      to_number + rhs
    else
      plus(rhs)
    end
  end
end
```

And to continue, we'll redefine `+` on String.
If it's numberwang, we'll convert it to a number and call `+` on that.
Otherwise, we'll use the `plus` alias to preserve the normal behaviour.

So that's all a bit confusing, but it does work:

----

```ruby
1 + "2"       # => 3

"1" + 2       # => 3

"1" + "2"     # => 3

"foo" + "bar" # => "foobar"
```

It works if one or both values are numberwang, and you can still concatenate
regular strings together.
Job done! Well, not quite.

----

```ruby
"foo" + 1
# => TypeError: can't convert Fixnum into String

1 + "foo" # => 1
```

We still can't add dissimilar objects.
You might think that's sensible.
What would the MySQL developers do?
They wouldn't let a nonsensical operation raise an error.
Let's turn the numbers into strings!

----

```ruby
class String
  def coerce(lhs)
    if numberwang?
      [lhs, to_number]
    else
      [lhs.to_s, self]
    end
  end
end
```

We'll branch in coerce: if the string looks like a number, we'll convert it to
a number; otherwise, we'll convert the left hand side to a string.

In order to avoid infinite recursion, however, we need to be a bit cleverer
in `String#+`.

----

```ruby
class Object
  def numberwang?; false; end
end

class Numeric
  def numberwang?; true; end
end

class String
  def +(rhs)
    if numberwang? && rhs.numberwang?
      to_number + rhs
    else
      plus(rhs.to_s)
    end
  end
end
```

We'll only try to add if _both_ sides are numberwang, so we'll define that
method on `Object` and `Numeric` as well.

----

```ruby
1 + "2"       # => 3

"1" + 2       # => 3

"1" + "2"     # => 3

"foo" + "bar" # => "foobar"

"foo" + 1     # => "foo1"

1 + "foo"     # => "1foo"
```

Look at that! Isn't that beautiful?
Just think of how clean your Rails app is going to look now!

We're still not done, though.
Look:

----

```ruby
1.2 + "3.4" # => "1.23.4"
```

(Sad trombone).
That is not an acceptable result.
We forgot about floats.
Let's fix those, too.

----

```ruby
class String
  def numberwang?
    match(/\A(?:\d+(?:\.\d+)?|0x[0-9a-f]+)\z/)
  end

  def to_number
    match(/\./) ? Float(self) : Integer(self)
  end
end
```

We'll upgrade `numberwang?` to make it recognise floats (and hex while we're at
it), and redefine the `to_number` method we conveniently happened to alias
earlier to use `Float()` or `Integer()` to do the conversion.

Et voilà:

----

```ruby
1.2 + "3.4"   # => 4.6

"1.2" + 3.4   # => 4.6

"1.2" + "3.4" # => 4.6

0x10 + "0x08" # => 24
```

Fantastic.
That's now working as expected, and we can add hex numbers as well as floats.
Let's see if we can do subtraction, too.

----

```ruby
class String
  def -(rhs)
    if numberwang? && rhs.numberwang?
      to_number - rhs
    else
      raise TypeError, "can't subtract non-numeric strings"
    end
  end
end
```

Does that work?

----

```ruby
3 - "2"   # => 1

"3" - 2   # => 1

"3" - "2" # => 1

"foo" - "bar"
# => TypeError: can't subtract non-numeric strings
```

Looks good to me.

Should you do this? Probably not.

Now go forth, pollute your codebases, make your colleagues hate you, and have
fun.
