# 1 + 2 = 3

My LRUG presentation for 11 February 2013.

## Requirements

* An ANSI-capable terminal emulator.
* Ruby 1.9.2+ (2.0.0-rc2 works too!)

## How to run it

* Open a terminal window of at least 70x20 characters and enlarge the font.
* Run `ruby runthru.rb`

You can customise the delay between slides by adding a parameter;

```sh
ruby runthru.rb 2
```

will run with 2s per slide instead of the default 20s.

## Evaluator

This will read a Markdown file, evaluate anything marked as Ruby via backtick
blocks, and emit the file with `# =>` replaced with the result of the preceding
expression:

```sh
ruby evaluate.rb talk.md
```

I use it to ensure that the results in `talk.md` are correct, by binding it to
the `,r` key sequence in Vim:

    :map ,r :w<CR>:1,$!ruby evaluate.rb<CR>
