pause = (ARGV.first || 20).to_i

class Slide
  def initialize(raw)
    if raw =~ /\`\`\`ruby\n(.*?)\`\`\`/m
      slide = $1
    else
      slide = raw.strip.split(/\n\n/).first
    end
    @lines = slide.split(/\n/)
  end

  def center(width, height)
    v_indent = (height - self.height) / 2
    h_indent = (width - self.width) / 2
    "\n" * v_indent + @lines.map { |l| " " * h_indent + l }.join("\n")
  end

  def width
    @lines.map(&:length).max
  end

  def height
    @lines.length
  end
end

def clear_screen
  print "\e[H\e[2J"
end

def move_cursor(x, y)
  print "\e[%s;%dH" % [y, x]
end

markdown = File.open("talk.md", "r:UTF-8").read
slides = markdown.split(/^----$/).map { |s| Slide.new(s) }
max_height = slides.map(&:height).max
max_width = slides.map(&:width).max
t0 = Time.now
$stdout.sync

slides.each_with_index do |slide, i|
  t1 = t0 + pause * i
  clear_screen
  puts slide.center(max_width, max_height)
  move_cursor 1, max_height + 2
  while Time.now < (t1 + pause)
    print "\r%d/%d %ds" % [i+1, slides.length, Time.now - t1]
    sleep 0.1
  end
end

puts
